#include <iostream>
#include <string>

using namespace std;

class Buch {
string autor; string titel; unsigned int inventarnummer; unsigned int benutzernummer=0;

public:
Buch (string autor, string titel, unsigned int inventarnummer);
bool istVerliehen ();
unsigned int inventarNummer ();
unsigned int ausgeliehenVon ();
bool verleiheAn (unsigned int benutzernummer);
void rueckgabe ();
void print ();
};