#include <iostream>
#include <string>
#include "buch.h"

using namespace std;


Buch::Buch(string autor, string titel, unsigned int inventarnummer) {
    this->autor = autor;
    this->titel = titel;
    this->inventarnummer = inventarnummer;
}

bool Buch:: istVerliehen () {
    if (benutzernummer==0) {
        return false;
    }
    return true;

}
unsigned int Buch:: inventarNummer () {
    return inventarnummer;
}

unsigned int Buch:: ausgeliehenVon () {
    return benutzernummer;
}
bool Buch:: verleiheAn (unsigned int benutzernummer) {
    if (!istVerliehen()) {
        this -> benutzernummer=benutzernummer;
        return true;
    }
    return false;
}
void Buch:: rueckgabe () {
    benutzernummer= 0;
}
void Buch:: print () {
    cout << "Buch - " << "Inventarnummer " << inventarnummer <<  ": " << titel;
    if (istVerliehen()) {
        cout << ", ausgeliehen von Benutzer " << benutzernummer << endl;
    } else {
        cout << ", verfügbar" << endl;
    }
}